from __future__ import unicode_literals

from django.db import models
from slack.models import SlackUser


class TravelLocation(models.Model):
    name = models.CharField(max_length=255)


class TravelTime(models.Model):
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()


class TravelInfo(models.Model):
    location = models.ForeignKey(TravelLocation)
    time = models.ForeignKey(TravelTime)
    is_travelling = models.BooleanField(default=False)
    user = models.ForeignKey(SlackUser, related_name='travels')
