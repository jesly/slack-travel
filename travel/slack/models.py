from __future__ import unicode_literals

from django.db import models


class SlackUserManager(models.Manager):

    def create_user(self, user_id, user_name):
        if not self.filter(user_id=user_id).exists():
            return self.create(user_id=user_id, user_name=user_name)
        return self.get(user_id=user_id)


class SlackTeamManager(models.Manager):

    def create_team(self, team_id, team_domain):
        if not self.filter(team_id=team_id).exists():
            return self.create(team_id=team_id, team_domain=team_domain)
        return self.get(team_id=team_id)


class SlackChannelManager(models.Manager):

    def create_channel(self, channel_id, channel_name, team):
        if not self.filter(channel_id=channel_id).exists():
            return self.create(channel_id=channel_id,
                               channel_name=channel_name,
                               team=team)
        return self.get(channel_id=channel_id)


class SlackTeam(models.Model):
    team_id = models.CharField(max_length=255)
    team_domain = models.CharField(max_length=255)
    objects = SlackTeamManager()


class SlackChannel(models.Model):
    channel_id = models.CharField(max_length=255)
    channel_name = models.CharField(max_length=255)
    objects = SlackChannelManager()
    team = models.ForeignKey(SlackTeam, related_name='channels')


class SlackUser(models.Model):
    user_id = models.CharField(max_length=255)
    user_name = models.CharField(max_length=255)
    objects = SlackUserManager()
    teams = models.ManyToManyField(SlackTeam, blank=True, related_name='users')
    channels = models.ManyToManyField(SlackChannel, blank=True,
                                      related_name='users')
