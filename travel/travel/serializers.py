import re
import time
import datetime
from rest_framework import serializers
from slack.models import SlackChannel, SlackTeam, SlackUser
from travelling.models import TravelLocation, TravelTime, TravelInfo


class SlackTravelWhoSerializer(serializers.Serializer):
    team_id = serializers.CharField(write_only=True)
    team_domain = serializers.CharField(write_only=True)
    channel_id = serializers.CharField(write_only=True)
    channel_name = serializers.CharField(write_only=True)
    user_id = serializers.CharField(write_only=True)
    user_name = serializers.CharField(write_only=True)
    command = serializers.CharField(write_only=True)
    text = serializers.CharField(write_only=True)


class SlackTravelOnSerializer(serializers.Serializer):
    team_id = serializers.CharField(write_only=True)
    team_domain = serializers.CharField(write_only=True)
    channel_id = serializers.CharField(write_only=True)
    channel_name = serializers.CharField(write_only=True)
    user_id = serializers.CharField(write_only=True)
    user_name = serializers.CharField(write_only=True)
    command = serializers.CharField(write_only=True)
    text = serializers.CharField(write_only=True)

    def create(self, validated_data):
        team_id = validated_data.get('team_id', None)
        team_domain = validated_data.get('team_domain', None)
        channel_id = validated_data.get('channel_id', None)
        channel_name = validated_data.get('channel_name', None)
        user_id = validated_data.get('user_id', None)
        user_name = validated_data.get('user_name', None)
        team = SlackTeam.objects.create_team(team_id, team_domain)
        channel = SlackChannel.objects.create_channel(
            channel_id, channel_name, team)
        user = SlackUser.objects.create_user(user_id, user_name)
        user.teams.add(team)
        user.channels.add(channel)
        user.save()
        command = validated_data.get('command', None)
        text = validated_data.get('text', None)
        if command == '/travel':
            travel_data = self.match_travel_to_command(text)
            location = travel_data['location']
            day = travel_data['day']
            month = travel_data['month']
            year = datetime.datetime.now().year
            date_string = "%s %s %d" % (day, month, year)
            start_date = datetime.datetime.strptime(
                date_string, "%d %b %Y").date()
            end_date = start_date + datetime.timedelta(days=12)
            travel_time = TravelTime.objects.create(
                start_date=start_date, end_date=end_date)
            travel_location = TravelLocation.objects.create(name=location)
            info = TravelInfo.objects.create(location=travel_location,
                                             time=travel_time,
                                             user=user)
            return info

    def match_travel_to_command(self, text):
        regex = re.compile(
            '^(on)\s*(Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Dec)?\,?\s*(\d{1,2})?\s*(to)?\s*([a-zA-z,\s*]+)')
        match = regex.match(text)
        print match.groups()
        if match is not None:
            if match.group(1) == 'on':
                # command
                month = match.group(2)
                day = match.group(3)
                if match.group(4) == 'to':
                    # date
                    location = match.group(5)
                    return {'location': location,
                            'day': day,
                            'month': month
                            }
                return None
            return None
        return None
