from django.conf.urls import include, url
from django.contrib import admin
from travel.views import SlackEntryView
urlpatterns = [
    # Examples:
    # url(r'^$', 'travel.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^slack-entry', SlackEntryView.as_view()),
]
