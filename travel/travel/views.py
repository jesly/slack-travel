import re
import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FormParser
from travel.serializers import SlackTravelOnSerializer
from travelling.models import TravelInfo


class SlackEntryView(APIView):
    parser_classes = (FormParser,)

    def post(self, request, format=None):
        regex = re.compile('^(\w+)')
        print request.data
        text = request.data.get('text')
        print 'Text:' + text
        command_identifier = None
        if regex.match(text) is None:
            command_identifier == 'list'
        else:
            command_identifier = regex.match(text).group(1)

        if command_identifier == 'on':
            serializer = SlackTravelOnSerializer(data=request.data)
            if serializer.is_valid():
                object = serializer.save()
                travelling_string = "Travelling to %s on %s", (
                    object.location.name, object.time.start_date)
                data = {'text': 'You have been added to travel list',
                        "attachments": [
                            {
                                "text": travelling_string
                            }
                        ]}
            return Response(data=data, status=status.HTTP_200_OK)
        else:
            data = ""
            for travel_info in TravelInfo.objects.all():
                data += "*%s* _is travelling_ to *%s* on *%s*\n" % (travel_info.user.user_name,
                                                                    travel_info.location.name,
                                                                    travel_info.time.start_date.strftime("%d, %b"))
            send_data = {'text': 'These are the people on travel',
                         "attachments": [
                                 {
                                     "text": data,
                                     "mrkdwn_in": ["text", ]
                                 }
                         ]
                         }
            return Response(data=send_data, status=status.HTTP_200_OK)
